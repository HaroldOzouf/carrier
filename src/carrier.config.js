(function () {

  angular
    .module('carrier')
    .config(configure);

  configure.$inject = ['$urlRouterProvider', '$locationProvider', '$httpProvider'];

  function configure($urlRouterProvider, $locationProvider, $httpProvider) {
    $locationProvider.html5Mode(false);

    $urlRouterProvider.otherwise('/containers');
  }

})();
