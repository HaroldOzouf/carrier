'use strict';

angular
  .module('carrier')
  .filter('capitalize', function () {
    return function (itm) {
      if (itm) {
        return itm.charAt(0).toUpperCase() + itm.slice(1);
      } else {
        return '';
      }
    };
	});
