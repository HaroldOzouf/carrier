(function () {
  'use strict';

  angular
    .module('carrier')
    .controller('RootController', RootController);

  RootController.$inject = ['$scope', '$state'];

  function RootController($scope, $state) {
    $scope.$state = $state;

    $scope.menu = [
      { name: 'containers', icon: 'cubes', state: 'containers' },
      { name: 'diagram', icon: 'sitemap', state: 'diagram' }
    ];

  }

})();
