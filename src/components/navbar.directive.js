(function () {

  angular
    .module('carrier')
    .directive('caNavbar', caNavbar);

  caNavbar.$inject = ['$state'];

  function caNavbar($state) {
    return {
      restrict: 'E',
      replace: true,
      templateUrl: 'app/components/navbar.directive.html',
      scope: {
        menu: '='
      },
      link: function (scope, el, attrs) {

        scope.isActive = function (menuItem) {
          return $state.includes(menuItem.state) ? 'active item' : 'item';
        }

      }
    };
  }



})();
