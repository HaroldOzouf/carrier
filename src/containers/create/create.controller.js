


(function () {

  'use strict';

  angular
    .module('carrier')
    .controller('ContainersCreateController', ContainersCreateController);

  ContainersCreateController.$inject = [
    '$scope', 
    '$state', 
    '$stateParams', 
    'regexp', 
    'containers'
  ];

  function ContainersCreateController($scope, $state, $stateParams, regexp, containers) {

    $scope.gitRepositoryRegexp = regexp.gitRepository;
    $scope.name = '';
    $scope.container = {
      links: [],
      ports: [],
      volumes: [],
      repositories: []
    };

    $scope.availableLinks = [];

    $scope.create = create;
    $scope.update = update;
    $scope.cancel = cancel;
    $scope.remove = remove;
    $scope.addVolume = addVolume;
    $scope.addPort = addPort;
    $scope.addRepository = addRepository;
    $scope.removeVolume = removeVolume;
    $scope.removePort = removePort;
    $scope.removeLink = removeLink;

    $scope.$watch('newLink', function (newLink, oldLink) {
      if (oldLink !== newLink && newLink) {
        $scope.container.links.push(newLink);
        $scope.availableLinks.splice($scope.availableLinks.indexOf(newLink), 1);
        $scope.newLink = '';
      }
    });

    init();

    function init() {
      if ($state.is('containers.edit')) {
        $scope.name = $stateParams.name;
        
        containers
          .get($scope.name)
          .then(initContainer)
          .catch(handleErrors); 
      } else {
        containers
          .get()
          .then(addToAvailableLinks);
      }
    }

    function initContainer(data) {
      $scope.container = data.container;
      containers
        .get()
        .then(addToAvailableLinks);
    }
    
    function addToAvailableLinks(data) {
      for (name in data.containers) {
        if (name !== $scope.name && !(name in $scope.container.links)) {
          $scope.availableLinks.push(name);
        }
      }
    }
    
    function handleErrors(error) {
      var message;
        
      switch (error.code) {
        case 'NAME_ALREADY_EXIST':
          message = 'A container already has this name';
        case 'IMAGE_NOT_PROVIDED':
          message = 'You must provide a docker image';
          break;
        default:
          message = 'Something wrong happened';
      }
      toastr.error(message);
    }

    function create() {
      containers
        .add($scope.name, $scope.container)
        .then(go('containers'))
        .catch(handleErrors);
    }
    
    function update() {
      containers
        .update($scope.name, $stateParams.name, $scope.container)
        .then(go('containers'))
        .catch(handleErrors);
    }
    
    function cancel() {
      $state.go('containers');
    }

    function remove() {
      containers
        .remove($scope.name)
        .then(go('containers'))
        .catch(handleErrors);
    }
    
    function go(where, args) {
      return function () {
         $state.go(where, { reload: true });
      };
    } 
    
    function addVolume() {
      if ($scope.newVolumeRepository) {
        var repository = $scope.container.repositories.filter(function (repository) { 
          return repository.name === $scope.newVolumeRepository; 
        })[0];
        
        $scope.container.volumes.push({
          repository: repository,
          from: $scope.newFrom,
          to: $scope.newTo
        });

        $scope.newTo = $scope.newFrom = $scope.newVolumeRepository = '';
      } else {
        toastr.error('You must provide a source repository');
      }
    }

    function addRepository() {
      var repositoryUrl = $scope.gitRepositoryRegexp.exec($scope.newRepository),
          repository = {
            name: repositoryUrl[6],
            url: $scope.newRepository
          };

      if (!$scope.container.repositories.filter(function (elem) {
        return repository.name === elem.name;
      }).length) {
        $scope.container.repositories.push(repository);
        $scope.newRepository = '';
      } else {
        toastr.error('This repository has already been added');
      }
    }

    function addPort() {
      $scope.container.ports.push($scope.newPort);
      $scope.newPort = '';
    }

    function removeVolume(volume) {
      $scope.container.volumes.splice($scope.container.volumes.indexOf(volume), 1);
    }

    function removePort(port) {
      $scope.container.ports.splice($scope.container.ports.indexOf(port), 1);
    }

    function removeLink(link) {
      $scope.availableLinks.push(link);
      $scope.container.links.splice($scope.container.links.indexOf(link), 1);
    }

  }

})();

