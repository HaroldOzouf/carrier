
(function () {
  'use strict';

  angular
    .module('carrier')
    .controller('ContainersController', ContainersController);

  ContainersController.$inject = ['$scope', '$state', 'containers'];

  function ContainersController($scope, $state, containers) {
    console.log('containers');
    $scope.containers = [];
    $scope.selectedContainer = undefined;
    $scope.edit = edit;

    init();

    function init() {
      console.log();
      containers
        .get()
        .then(function (data) {
          $scope.containers = data.containers;
        });
    }

    function edit(name) {
      $state.go('containers.edit', { name: name });
    }
  }

})();
