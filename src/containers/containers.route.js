(function () {
  'use strict';

  angular
    .module('carrier')
    .config(configure);

  configure.$inject = ['$stateProvider'];

  function configure($stateProvider) {
    $stateProvider
        .state('containers', {
          url: '/containers',
          templateUrl: 'app/containers/containers.html',
          controller: 'ContainersController'
        })
        .state('containers.create', {
          parent: 'containers',
          url: '/create',
          templateUrl: 'app/containers/create/create.html',
          controller: 'ContainersCreateController'
        })
        .state('containers.edit', {
          parent: 'containers',
          url: '/edit/:name',
          templateUrl: 'app/containers/create/create.html',
          controller: 'ContainersCreateController'
        });
  }
})();
