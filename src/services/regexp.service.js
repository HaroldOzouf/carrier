(function () {
  'use strict';
  
  angular
    .module('carrier')
    .factory('regexp', regexpService);
  
  function regexpService() {
    return {
      gitRepository: /((git|ssh|http(s)?)|(git@[\w\.]+)):(\/\/)?[\w\.@\:\/\-~]+\/([\w@\:\-~]+)\.git\/?/      
    };
  }
  
})();