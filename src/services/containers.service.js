(function () {
  'use strict';

  angular
    .module('carrier')
    .factory('containers', containersService);

  containersService.$inject = ['$q'];

  function containersService($q) {
    var CONTAINERS_PROPERTY_NAME = 'ca-containers',
        containers = getContainersFromLocalStorage() || {};

    return {
      add: add,
      update: update,
      remove: remove,
      get: get
    };

    function add(name, container) {
      var deferred = $q.defer();

      if (containers[name]) {
        deferred.reject({ code: 'NAME_ALREADY_EXIST' });
      } else if (!container['image']) {
        deferred.reject({ code: 'IMAGE_NOT_PROVIDED' });
      } else {
        containers[name] = {
          image: container.image,
          repositories: container.repositories,
          ports: container.ports || [],
          links: container.links || [],
          volumes: container.volumes || [],
          command: container.command || ''
        };
        saveContainersInLocalStorage();
        deferred.resolve({
          name: name,
          container: containers[name]
        });
      }

      return deferred.promise;
    }

    function get(name) {
      var deferred = $q.defer();

      if (!name) {
        deferred.resolve({
          containers: containers
        });
      } else if (!containers[name]) {
        deferred.reject({ code: 'UNKNOWN_CONTAINER' });
      } else {
        deferred.resolve({
          name: name,
          container: containers[name]
        });
      }
      return deferred.promise;
    }

    function remove(name) {
      var deferred = $q.defer();

      if (!containers[name]) {
        deferred.reject({ code: 'UNKNOWN_CONTAINER' });
      } else {
        delete containers[name];
        saveContainersInLocalStorage();
        deferred.resolve({ name: name });
      }

      return deferred.promise;
    }

    function update(name, oldName, container) {
      var deferred = $q.defer();
      
      if (name !== oldName) {
        containers[name] = containers[oldName];
        delete containers[oldName];
      }
      
      if (!containers[name]) {
        deferred.reject({ code: 'UNKNOWN_CONTAINER' });
      } else if (!container['image']) {
        deferred.reject({ code: 'IMAGE_NOT_PROVIDED' });
      } else {
        $.extend(containers[name], container);
        saveContainersInLocalStorage();

        deferred.resolve({
          name: name,
          container: containers[name]
        });
      }

      return deferred.promise;
    }


    function getContainersFromLocalStorage() {
      return JSON.parse(localStorage.getItem(CONTAINERS_PROPERTY_NAME));
    }

    function saveContainersInLocalStorage() {
      localStorage.setItem(CONTAINERS_PROPERTY_NAME, JSON.stringify(containers));
    }
  }
})();
